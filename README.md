# Django Bootstrapping Done Right

## Credits

- Fernando Aramendi (fernando@devartis.com / tw: @faramendi)
    - Software Engineer
    - Managing Partner at devartis
- Pablo García (poli@devartis.com / tw: @poligarcia)
    - Software Engineer
    - Managing Partner at devartis
- Many, many contributions from @devartis team // [devartis.com](https://devartis.com)


## Introduction

We have been working with Django for many years and we discovered we were always repeating the same things at the beginning of each project. Codestyling checks, unit tests, automated deployment: we where doing all of them over and over again and occasionally we were missing important things that would make our lives easier.

That's why we decided to create and maintain template repository with all the required tools to bootstrap a Django project. It has become very valuable for our company and that's why we want to share it with the community. The project is now open source and we will review all it does in this tutorial.

## Requirements

In order to use this project you'll need to have the following tools installed in your development box.

* git
* Docker & Docker Compose
* Python 3
* Node.js
* Your favorite source-code editor or IDE
* Bitbucket **free** account: We will use Bitbucket's pipelines
* Heroku **free** account

### Docker

[docker](https://www.docker.com/) is a tool for running containers based on pre-built images that will help us launch the different environments we need for our development process. Search for the proper installation instructions depending on your OS here https://docs.docker.com/install/. When installed, validate it was installed properly by running:

Please check your docker install by running in your console `docker -v`. You should expect to see something similar to:

```bash
$ docker -v
Docker version 19.03.1, build 74b1e89
```

[docker-compose](https://docs.docker.com/compose/) will be required to launch multiple containers at the same time. Check for the proper installation instructions here https://docs.docker.com/compose/install/

Please check your docker-compose install by running in your console `docker-compose -v`. You should expect to see something similar to:

```bash
$ docker-compose -v
docker-compose version 1.24.1, build 4667896b
```

### Python 3

Python2 will be sunsetting [in 2020](https://www.python.org/dev/peps/pep-0373/#update) so using Python 3 is a must. Python 3 should come pre-installed with every modern operating system. You can validate it is installed by running:

`python3 --version` or `python --version`

The expected output should be something like:

```
$ python3 --version
Python 3.6.6
```

Note: The default python interpreter might be set to python 2. This doesn't matter as long as Python 3 is also installed.

For this workshop we won't care about Python 3 minor versions. But if you'd like to install a particular Python version together with other versions in the same machine then you might consider using [`pyenv`](https://github.com/pyenv/pyenv). This step is not required.

1. Use [pyenv-installer](https://github.com/pyenv/pyenv-installer) for installing `pyenv`.
1. See which python versions are available: `pyenv install --list`.
1. Install python 3. Example: `pyenv install 3.6.6` (3.6.6 or higher).

### Node.js

Also [nodejs](https://nodejs.org/en/) is required for installing and running some tools we will use during the development process.

1. Install `nodejs`, this could be achieved by using [nvm](https://github.com/creationix/nvm).
1. Install `nodejs` version `7` or later.
1. Check the install by running `npm --version`. You should see an output similar to:

```
$ npm --version
6.5.0
```


### Python IDE / Text editor

You can use any editor that you like. Here are a couple of choices:

* Pycharm
* Visual Studio Code
* SublimeText
* Atom

The only real IDE of the list is Pycharm which has very useful debugging tools. There's a free community edition which is great for Django development at: https://www.jetbrains.com/pycharm/download/#section=linux

## Get the code: Fork this repository (or don't)

First thing we want to do is to copy all files included here to your own project.

A way to do this is to fork the repository. For this you need to click on the **+** sign on the left menu and then on _Fork this repository_.

Unfortunately Bitbucket doesn't let us [detach the fork relationship](https://bitbucket.org/site/master/issues/13645/detach-fork-from-upstream-repository?_ga=2.124476746.1157251317.1554214266-698969354.1525956740) so your repository will _always_ be related to this original repository.

If you want to avoid this _level of commitment_ you can clone the project and change the `origin` remote:

```bash
$ git clone git@bitbucket.org:devartis/django-bootstrap.git
$ cd django-bootstrap
$ git remote rename origin old
$ git remote add origin <your git repository URL>  # Note: This repository should be empty
$ git push -u origin master
```

And if you want to remove this project's history and start with only one inital commit you'll need to follow these steps:

```bash
$ git clone git@bitbucket.org:devartis/django-bootstrap.git
$ cd django-bootstrap
$ rm -rf .git
$ git init .
$ git add .
$ git commit -m "Adding devartis' awesome bootstrap project."
$ git remote add origin <your git repository URL>  # Note: This repository should be empty
$ git push -u origin master
```

Login to your Bitbucket account and make sure your new repository is there as intended.

## Dependency Management and Virtual Environment - Pipenv!

[Pipenv](https://pipenv.readthedocs.io/en/latest/) _automatically creates and manages a virtualenv for your projects, as well as adds/removes packages from your Pipfile as you install/uninstall packages. It also generates the ever-important Pipfile.lock, which is used to produce deterministic builds._

Pipenv is a package management tool [recommended by Python](https://packaging.python.org/tutorials/managing-dependencies/#managing-dependencies). [Some people](https://chriswarrick.com/blog/2018/07/17/pipenv-promises-a-lot-delivers-very-little/) think they can live without it, but we will give it a try.

First, let's install `pipenv` using `pip`:

```bash
$ pip install --user pipenv  # the --user is optional, if you do please verify the user's bin directory is in your path
```

Things we can try with `pipenv` installed:

```bash
$ python --version  # What's my python version? This is not pipenv's python!
$ pipenv --three --python=<path-to-python-optional>  # Let's create a python3 env
$ pipenv shell  # Let's "activate" our python
$ python --version  # Now we're using pipenv's python
$ exit  # pipenv shell spawns a new shell session and activates the venv!
$ pipenv --rm  # Delete the virtualenv.
```

Now let's install our project requirements:

```bash
$ pipenv install --dev --python=<path-to-python-optional>
$ pipenv shell
$ python 
>>> import django
>>> django.get_version()
'2.2.3'
>>> exit()
```

### Troubleshooting AttributeError: 'NoneType' object has no attribute 'pythons'

Please see https://github.com/pypa/pipenv/issues/3439#issuecomment-456397076.

## Project Structure

### Settings

We created multiple settings files for the different environments. Take a look at the settings directory.

This project adopts [The 12 factor methodology](https://12factor.net/), hence, one thing we pay attention to is to have all the settings defined by environment variables [(Factor III: _Store config in the environment_)](https://12factor.net/config) instead of hardcoding passwords or sensitive information in versioned files.

Please review `conf/settings/base.py`, `productiuon.py` and `local.example.py`. Take a look specially at how the `DATABASES` setting is defined. This setting is defined as an environment variable called `DATABASE_URL` (see `conf/settings/.env.sample`) that [`django-environ`](https://django-environ.readthedocs.io/) turns into the database configuration that django requires.

To apply this on our development environment we'll need to copy the two sample files:

```bash
cp conf/settings/local.example.py conf/settings/local.py 
cp conf/settings/.env.sample conf/settings/.env
```

## Docker intro

_A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings._

We will use Docker to run external dependencies of our project (like a webserver, database or a cache) and also to debug Pipelines.

To start a container with python you can run:

```bash
$ docker run -it --entrypoint=/bin/bash python:3.6.6
<Lots of docker output>
root@<container ID>:/# python --version
Python 3.6.6
root@<container ID>:/# exit
```

And you can run a MySQL container by running:

```bash
$ docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=aN0tSoStrongP4ss -e MYSQL_DATABASE=db mysql
<Lots of docker output>
<Lots of MySQL output>
```

On a different terminal, you can connect to the running MySQL engine using: 

```bash
$ mysql -h127.0.0.1 -uroot -paN0tSoStrongP4ss
mysql> show databases;
mysql> exit;
```

You may also start the containers using the `-d` flag. This starts the containers in daemon mode.

### Exercise

Start a MySQL instance using Docker and then run your Django application using it as database. Try to log in to the admin site.

## Introducing docker-compose

There’s an easier way to run multiple containers at the same time and relate them, with the [Docker Compose](https://docs.docker.com/compose/) tool.

The `docker-compose.yml` file defines the configuration of different services and how they relate to each other. This project comes configured with a PostgreSQL database and a Python service that runs your Django app.

The `docker-compose` script has different commands to help you manage the services that are part of your configuration. Some of them are:

- `docker-compose ps`: displays the status of the services.
- `docker-compose build`: Build the images for the containers that will run your services (more on this later).
- `docker-compose up`: starts the services in daemon mode.
- `docker-compose stop`: stops the services, leaving the data of the containers unaltered.
- `docker-compose down`: stops the services and destroys the containers. Don't confuse `stop` and `down` commands or you will end up losing data!
- `docker-compose logs <services names>`: displays the logs of the running services.
- `docker-compose exec <service name> <command>`: This is a really useful command to work with your containers. It lets you run the programs that are included in your container, in its excution context.

### Exercise

Start the services of this project in daemon mode. See the status of execution of them using the `ps` and `logs` commands. Use the `exec` command to run `python manage.py shell` inside the container running the Django service (hint: remember `pipenv`). Finally, cleanup the containers using the `down` command.

## Setup a development Database

If we try to launch Django application we'll get an error since the database is not yet configured:

```bash
$ python manage.py runserver_plus
```

If you didn't copy the `.env.sample` file you'll get a `Set the DATABASE_URL environment variable` error. If you did, you'll most likely get a `Connection refused` since we don't have any DB engine running yet.

```
$ docker-compose up -d db
$ python manage.py runserver_plus
```

This will work, but you'll get some warnings on the Django shell. To solve them we need to run migrations against the configured database:

```bash
$ python manage.py migrate
$ python manage.py runserver_plus
```

You should have a running Django project to this point on http://localhost:8000/.

## Development with Docker

Let's discuss about the development process of a Django application using containers. By setting up everything using Docker containers, you are forcing your development team to think about different problems the app will face when deployed on a production environment:
- Which environment variables should I set?
- What operating system dependencies my app needs?
- Which are the other services my app relies on?
- How will the python process will be run?

You may use run the development version of the app using `manage.py runserver` from the shell or using the containerized version, it's up to you!

### Exercise

Let's see how it feels to run your development environment using docker:

1. Build django image: `docker-compose build`.
1. Start the services: `docker-compose up -d`.
1. Migrate the database: `docker-compose run --rm django pipenv run python3 manage.py migrate`.
1. Create a super user: `docker-compose run --rm django pipenv run python3 manage.py createsuperuser`.
1. Restart django: `docker-compose restart django`.
1. Make a modification on a python file from the django project. Did the app reload automatically?

## Git hooks

Git hooks are scripts that Git executes before or after specific events such as: commit, push, and receive. Git hooks are a built-in feature, so all we need to do is to create a shell script.

The hooks configured for the project are:

* [Flake8](http://flake8.pycqa.org/en/latest/index.html).
* [Pylint](https://pylint.readthedocs.io/en/latest/).
* [Eslint](https://eslint.org/).
* [Jscpd](https://github.com/kucherenko/jscpd).

Since some linters require [nodejs](https://nodejs.org/en/), please install nodejs and then run `npm install`.

Now we can configure the hooks:

```bash
$ ls -la .git/hooks
$ cp scripts/pre-push.sh .git/hooks/pre-push
$ chmod +x .git/hooks/commit-msg
$ cp scripts/commit-msg.sh .git/hooks/commit-msg
$ chmod +x .git/hooks/commit-msg
```

### Exercise

Force a `pre-push` failure by inserting a PEP8 violation (add multiple line breaks at the end of `project/apps/common/views.py`).

## Pipelines (Continuous Integration)

### Enable bitbucket pipelines

Bitbucket now has Pipeline runners for free but it's limited to 50h a month. In any case, the pipelines functionality can be migrated to github+travis or gitlab+gitlab-ci (coming soon).

Go to the Pipelines section of your repository in Bitbucket and click on the "Start using pipelines" link on the bottom, and then the green "Emnable" button.

Click again on the "Pipelines" link on the left navigation of your Bitbucket project and check that it is running for the first time. Click on the blue "In Progress" link. All steps should be green!

Now let's look at the `bitbucket-pipelines.yml` file.

### How to debug a pipeline

It's very likely that you'll want to modify pipelines over time or that eventually something will break. It's important to be able to debug things locally so that you don't have to push new commits to test runs.

Suppose that the "Pylint" step of the build failed. In this case you would like to execute the same script Bitbucket executed on an identicall environment.

To start a Python container that replicates that environment run:

```bash
$ docker run -it --volume=$(pwd):/django-bootstrap --workdir="/django-bootstrap" --memory=4g --memory-swap=4g --memory-swappiness=0 --entrypoint=/bin/bash python:3.6.6
```

Then run the failed script by running the same exact script that the _Pylint_ step of the build runs:

```bash
$ pip install --user pipenv
$ pipenv install --dev  # This step is cached because might take long
$ export PATH=$PATH:/root/.local/bin
$ pipenv run pylint --rcfile=scripts/.pylintrc project
```

## Deployment (Continuous Delivery)

### Heroku

For simplicity we will use Heroku as our application server.

Login in to Heroku and click New -> Create new app

Install Heroku cli following the instructions for your OS at https://devcenter.heroku.com/articles/heroku-cli

Heroku looks for a `Procfile` in order to understand how to run your app. Open it.

The easiest way to deploy to Heroku would be to push the code to its remote:

```bash
$ heroku login
$ heroku git:remote -a <your app name>
$ git push heroku master
```

This will fail because we have not set the required environment variables on Heroku. We will set them using heroku-cli, but you can also do it from the web interface:

```bash
$ heroku config:set DJANGO_SETTINGS_MODULE=conf.settings.production  # We will treat the Heroku app as a productive environment
$ heroku config:set DJANGO_SECRET_KEY="A-super-seret-key"
$ heroku config:set ALLOWED_HOST="your-heroku-app-name.herokuapp.com"
$ heroku config:set DATABASE_URL=<Get one from Resources > Add-ons on Heroku>
```

Now we can deploy again:

```bash
$ git push heroku master
```

But we want to configure a Pipeline to do this so that we can make it depend on succesfull build and also centralize deployment in our build server instead of one developer's computer.

First we need to generate an API token:

```bash
$ heroku authorizations:create -d "Bitbucket pipelines token"
Creating OAuth Authorization... done
Client:      <none>
ID:          <a UUID>
Description: Bitbucket pipelines token
Scope:       global
Token:       **<UUID corresponding to the API key>**
```

Set the token and application name as a variables in Bitbucket. In your project navigate to Settings -> Pipelines -> Repository Variables and set the two variables:

- `HEROKU_APP_ID`: The name of your application.
- `HEROKU_API_KEY`: The token generated in the previous step.

Test running the deploy on your pipeline by clicking on the "Run" button on the pipeline view.

#### Exercise

Perform a change on the application locally, commit and push the code, and after the pipeline successfully runs, deploy to Heroku.

## Monitoring your production app

### Sentry

[Sentry](https://sentry.io/) is an open-source error tracking platform that helps developers monitor and fix crashes in their applications. It's very handy to track errors thrown in production since it sends email digests, groups errors by type and gathers trace info.

There's a very easy way to start tracking your errors with Django and that's thanks to the [Raven client](https://github.com/getsentry/raven-python)
`production.py` settings already has raven configured, so all you'll need to do is to:

* Start a new Sentry project and get the DSN key.
* Set `RAVEN_DSN` env variable in the heroku environment.

### NewRelic

NewRelic is a great [application performance management](https://en.wikipedia.org/wiki/Application_performance_management) tool that's easy to plug and can become handy when you need to profile or monitor the performance of your application.

To configure it in Heroku is as simple as enabling the NewRelic agent addon and follow their instructions.

Installing the add-on automatically creates a private New Relic account and configures access for Heroku hosts. New Relic will begin monitoring application performance, end user experience, and host performance collected after the add-on is installed.
